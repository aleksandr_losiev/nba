import Vue from 'vue'
import Vuex from 'vuex'

import nba from './store/nba'
import popup from './store/popup'
import sorting from './store/sorting'
import filters from './store/filters'

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  modules: {
    nba,
    popup,
    sorting,
    filters
  }
})
