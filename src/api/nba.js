const axios = require('axios');

export class NbaAPI {
    getItems() {
        return axios.get('/api/nba.json')
            .then(function (response) {
                return response;
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}
