export default {
    methods: {
        addQueryToRoute(name, value) {
            let query = this.$route.query
            query[name] = value
            if (query) {
                window.history.replaceState(query, 'NBA', '?' +  this.objToUrl(query));
            }
        },
        removeQueryByRoute(name) {
            let query = this.$route.query

            if (query[name] !== undefined) {
                delete query[name]
            }

            window.history.replaceState('', 'NBA', '?' + this.objToUrl(query));

        },
        objToUrl(obj) {
            if (!Object.keys(obj).length) {
                return ''
            }

            return Object.entries(obj).map(([key, val]) => `${key}=${val}`).join('&')
        },
    }
}
