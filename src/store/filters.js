const state = {
    list: [
        {id: 1, name: 'All posts', type: 0},
        {id: 2, name: 'Video', type: 2},
        {id: 3, name: 'Photo', type:1},
    ],
    filterID: 1
}

const mutations = {
    setFilterID(state, id) {
        state.filterID = id
    }
}

const getters = {
    getActiveData(state) {
        return id => {
            return state.list.find(item => item.id === id)
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    getters
}
