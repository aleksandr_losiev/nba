const state = {
    list: [
        {id: 1, name: 'Not sorting', type: 0},
        {id: 2, name: 'ASC', type: 'asc'},
        {id: 3, name: 'DESC', type: 'desc'},
    ],
    sortID: 1
}

const mutations = {
    setSortID(state, id) {
        state.sortID = id
    }
}

const getters = {
    getActiveData(state) {
        return id => {
            return state.list.find(item => item.id === id)
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    getters
}
