import {NbaAPI} from '../api/nba'

const nba = new NbaAPI();

const state = {
    list: []
}

const mutations = {
    setList: (state, list) => {
        state.list = list
    }
}

const getters = {
    getItemById: state => id => {
        return state.list.find(item => item.id === id)
    }
}

const actions = {
    setItems(context) {
        nba.getItems().then(response => {
            if (response.status === 200) {
                let list = response.data,
                    newList = []

                for (let index in list) {
                    newList.push(list[index])
                }

                context.commit('setList', newList)
            }
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
}
