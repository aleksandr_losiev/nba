const state = {
    isOpen: false,
    idItem: null
}

const mutations = {
    setPopupStatus(state, status) {
        state.isOpen = status
    },
    setPopupItemKey(state, id) {
        state.idItem = id
    }
}

const actions = {
    closePopup(context) {
        context.commit('setPopupStatus', false)
        context.commit('setPopupItemKey', null)
    },
    openPopup(context, id) {
        // this.$router.replace({ name: 'home', query: { video: key }})
        context.commit('setPopupStatus', true)
        context.commit('setPopupItemKey', id)
    }

}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
